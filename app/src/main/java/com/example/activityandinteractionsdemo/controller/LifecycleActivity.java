package com.example.activityandinteractionsdemo.controller;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.activityandinteractionsdemo.databinding.ActivityLifecycleBinding;

public class LifecycleActivity extends AppCompatActivity {
    private ActivityLifecycleBinding binding;

    private int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLifecycleBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Log.d("LIFECYCLE", "onCreate: count = " + this.count);

        // On peut récupérer l'état des variables sauvegardées dans onSaveInstance
        if (savedInstanceState != null) {
            // peut être null, par exemple au premier lancement

            //
            // Commenter la ligne suivante pour ne PAS récupérer la valeur sauvegardée
            //
            this.count = savedInstanceState.getInt( "KEY_COUNT");
            binding.textCount.setText(String.valueOf(this.count));

            Log.d("LIFECYCLE", "savedInstanceState != null: count = " + this.count);
        }

        binding.buttonCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LifecycleActivity.this.count++;
                binding.textCount.setText(String.valueOf(LifecycleActivity.this.count));
            }
        });
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        Log.d("LIFECYCLE", "onSaveInstanceState: count = " + this.count);

        // On doit manuellement sauvegarder l'état des variables pertinentes
        outState.putInt("KEY_COUNT", this.count);

        super.onSaveInstanceState(outState);
        // Le contenu des composants d'interface qui possèdent un ID
        // est sauvegardé automatiquement
    }
}