package com.example.activityandinteractionsdemo.controller;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.activityandinteractionsdemo.databinding.ActivityIntentsBinding;
import com.example.activityandinteractionsdemo.model.User;

public class IntentsActivity extends AppCompatActivity {

    private ActivityIntentsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityIntentsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.buttonNewIntent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
                // Simple intent
                //
                Intent simpleIntent = new Intent(IntentsActivity.this, NewIntentActivity.class);
                IntentsActivity.this.startActivity(simpleIntent);
            }
        });

        binding.buttonIntentExtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
                // Intent with extras
                //
                Intent intentExtras = new Intent(IntentsActivity.this, NewIntentExtraActivity.class);
                intentExtras.putExtra("KEY_EXTRA_MESSAGE", binding.editTextMessage.getText().toString());
                IntentsActivity.this.startActivity(intentExtras);
            }
        });

        binding.buttonIntentParcelable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
                // Intent with complex data using Parcelable
                //
                User user = new User("John", "Doe", 1984);
                Intent intentParcelable = new Intent(IntentsActivity.this, NewIntentParcelableActivity.class);
                intentParcelable.putExtra("KEY_EXTRA_USER", user);// Could also be an array of Parcelable
                IntentsActivity.this.startActivity(intentParcelable);
            }
        });

        binding.buttonIntentForResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
                // Starting activity for result
                //

                // Using built-in contract
                //activityResultLauncher.launch(new Intent(IntentsActivity.this, NewIntentForResultsActivity.class));

                // Custom contract
                stringResultLauncher.launch(null);
            }
        });
    }

    // REF
    //      https://proandroiddev.com/is-onactivityresult-deprecated-in-activity-results-api-lets-deep-dive-into-it-302d5cf6edd
    //      https://wajahatkarim.com/2020/05/activity-results-api-onactivityresult/

    ActivityResultLauncher<Void> stringResultLauncher = registerForActivityResult(new CustomStringContract(),
        new ActivityResultCallback<String>() {
            @Override
            public void onActivityResult(String result) {
                if (result != null) {
                    binding.textResult.setText(result);
                } else {
                    binding.textResult.setText("Result cancelled!");

                }
            }
        }
    );

    ActivityResultLauncher<Intent> activityResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
        new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                int resultCode = result.getResultCode();
                Intent data = result.getData();

                Log.d("INTENTS", "ActivityResultCallback: " + resultCode);

                if (resultCode == Activity.RESULT_OK) {
                    binding.textResult.setText(data.getStringExtra("KEY_EXTRA_RESULT"));
                }  else {
                    binding.textResult.setText("Result cancelled!");
                }
            }
        }
    );

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Log.d("INTENTS", "onNewIntent");
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d("INTENTS", "onResume");
        binding.editTextMessage.setText(null);
    }
}

class CustomStringContract extends ActivityResultContract<Void, String> {
    @NonNull
    @Override
    public Intent createIntent(@NonNull Context context, Void input) {
        return new Intent(context, NewIntentForResultsActivity.class);
    }

    @Override
    public String parseResult(int resultCode, @Nullable Intent intent) {
        Log.d("INTENTS", "parseResult: " + resultCode);

        if (resultCode == Activity.RESULT_OK) {
            return intent.getStringExtra("KEY_EXTRA_RESULT");
        }

        return null; // cancelled
    }
}