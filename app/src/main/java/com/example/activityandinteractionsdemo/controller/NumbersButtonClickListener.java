package com.example.activityandinteractionsdemo.controller;

import android.view.View;
import android.widget.TextView;

public class NumbersButtonClickListener implements View.OnClickListener {
    private int value;
    private TextView text;

    public NumbersButtonClickListener(int value, TextView text) {
        this.value = value;
        this.text = text;
    }

    @Override
    public void onClick(View view) {
        text.setText("Number = " + this.value);
    }
}
