package com.example.activityandinteractionsdemo.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.activityandinteractionsdemo.R;

public class NewIntentExtraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_intent_extra);

        TextView text = (TextView)findViewById(R.id.textExtras);
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            String message = extras.getString("KEY_EXTRA_MESSAGE", "Nothing received...");
            text.setText(message);
        } else {
            text.setText("No extras...");
        }

        findViewById(R.id.buttonFinish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewIntentExtraActivity.this.finish();
            }
        });
    }
}