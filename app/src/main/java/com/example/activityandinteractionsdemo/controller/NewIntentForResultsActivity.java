package com.example.activityandinteractionsdemo.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.activityandinteractionsdemo.R;
import com.example.activityandinteractionsdemo.databinding.ActivityNewIntentForResultsBinding;

public class NewIntentForResultsActivity extends AppCompatActivity {

    ActivityNewIntentForResultsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityNewIntentForResultsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.buttonResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra("KEY_EXTRA_RESULT", binding.editTextResult.getText().toString());
                NewIntentForResultsActivity.this.setResult(Activity.RESULT_OK, resultIntent);

                NewIntentForResultsActivity.this.finish();
            }
        });
    }
}