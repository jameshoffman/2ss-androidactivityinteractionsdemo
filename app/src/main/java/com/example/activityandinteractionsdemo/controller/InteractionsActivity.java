package com.example.activityandinteractionsdemo.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

import com.example.activityandinteractionsdemo.R;
import com.example.activityandinteractionsdemo.databinding.ActivityInteractionsBinding;

public class InteractionsActivity extends AppCompatActivity implements TextWatcher {

    // aussi ajouter dans build.gradle du Module:app
    /*

    android {
        ...

        buildFeatures {
            viewBinding true
        }

        ...
    }
    */

    // L'ajout dans build.gradle génère une classe de binding pour chaque layout
    private ActivityInteractionsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityInteractionsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot()); // Lier le binding à l'affichage de l'activity

        // View Binding
        binding.textBinding.setText("Value from binding :)");

        // Find view by ID
        TextView text = findViewById(R.id.textFind);
        text.setText("Value from find!");
        // ((TextView)findViewById(R.id.textFind)).setText("Value from find!");

        // Event Listener classe anonyme inline
        binding.editTextInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                binding.textMirror.setText(editable.toString());
            }
        });

        // Event listener dans une classe anonyme stockée dans une variable membre de la classe
        binding.editTextInput.addTextChangedListener(this.variableAnonymousClass);

        // Une classe qui implemente l'interface du listener, ex: La classe actuelle -> this
        binding.editTextInput.addTextChangedListener(this);
        // Pourrait eventuellement être dans un classe distincte
        // binding.editTextInput.addTextChangedListener(new MyTextWatcherClass());

        // Réutilisation d'un listener via une classe distincte
        binding.buttonOne.setOnClickListener(new NumbersButtonClickListener(1, binding.textButtons));
        binding.buttonTwo.setOnClickListener(new NumbersButtonClickListener(2, binding.textButtons));
    }

    private TextWatcher variableAnonymousClass = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            binding.textMirrorAnonymous.setText(editable.toString().toUpperCase());
        }
    };

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        StringBuilder b = new StringBuilder();
        b.append(editable.toString());

        binding.textMirrorClass.setText(b.reverse());
    }
}