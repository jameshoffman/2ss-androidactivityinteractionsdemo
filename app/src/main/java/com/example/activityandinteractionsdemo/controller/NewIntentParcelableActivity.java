package com.example.activityandinteractionsdemo.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.activityandinteractionsdemo.R;
import com.example.activityandinteractionsdemo.model.User;

public class NewIntentParcelableActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_intent_parcelable);

        TextView text = findViewById(R.id.textParcelable);
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            User user = (User)extras.getParcelable("KEY_EXTRA_USER");
            text.setText(user.toString());
        } else {
            text.setText("No extras...");
        }
    }
}